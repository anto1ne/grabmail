#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 25 20:34:50 2021

@author: antoine
"""

import sys
import os
from PyQt5 import uic, QtCore
from PyQt5.QtWidgets import QWidget, QWizard, QPlainTextEdit, QVBoxLayout, QPushButton
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QIcon
import logging

from model.modules import get_data, save_list


class QTextEditLogger(logging.Handler, QtCore.QObject):
    appendPlainText = QtCore.pyqtSignal(str)

    def __init__(self, parent):
        super().__init__()
        QtCore.QObject.__init__(self)
        self.widget = QPlainTextEdit(parent)
        self.widget.setReadOnly(True)
        self.appendPlainText.connect(self.widget.appendPlainText)
    
    def emit(self, record):
        msg = self.format(record)
        self.appendPlainText.emit(msg)
        QtCore.QCoreApplication.processEvents()

class Mainwidget(QWizard):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        uic.loadUi('./view/gui.ui', self) 
        
        self.setWindowTitle("GrabMail")
        
        self.logTextBox=QTextEditLogger(self)
        #self.logTextBox=QPlainTextEdit(self)
        logging.getLogger().addHandler(self.logTextBox)
        logging.getLogger().setLevel(logging.INFO)
        layout = QVBoxLayout()
        layout.addWidget(self.logTextBox.widget)
        #layout.addWidget(self.logTextBox)
        self.boutonRecherche=QPushButton("Recherchez", self)
        self.boutonRecherche.setIcon(QIcon.fromTheme("edit-find"))
        layout.addWidget(self.boutonRecherche)
        self.wizardPage2.setLayout(layout)
            
        self.mails=[]
        #self.button(QWizard.NextButton).clicked.connect(self.next_btn)
        #self.currentIdChanged.connect(self.recherche)
        self.boutonRecherche.clicked.connect(self.recherche)
        self.pushButton.clicked.connect(self.save)
    
    def save(self):
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        file_name, _file_type = QFileDialog.getSaveFileName(
            self,
            "Choisissez l'emplacement",
            "",
            "CSV Files (*.csv);;All Files (*)",
            options=options)
        if file_name:
            name, extension = os.path.splitext(file_name)
            if extension == ".csv":
                filename = file_name
            else:
                filename = file_name + ".csv"
            save_list(filename, self.mails)


    def recherche(self):
        self.logTextBox.widget.setPlainText("")
        #self.logTextBox.setPlainText("")
        url = self.lineEdit.text()
        n = self.spinBox.value()
        
        urls=[url]
        mails=[]
        for n in range(0,n+1):
            logging.info("#Niveau : " + str(n))
            #self.logTextBox.appendPlainText("#Niveau : " + str(n))
            new_mail=[]
            new_urls=[]
            for link in urls :
                #print("Lien : ", link)
                urls_found, mails_found = get_data(link)
                new_mail.extend(mails_found)
                new_urls.extend(urls_found)
            logging.info("Nouveaux liens : " + str(len(new_urls)))
            #self.logTextBox.appendPlainText("Nouveaux liens : " + str(len(new_urls)))
            logging.info("Nouveaux mails : "+ str(len(new_mail)))
            #self.logTextBox.appendPlainText("Nouveaux mails : "+ str(len(new_mail)))
            logging.info("")
            #self.logTextBox.appendPlainText("")
            urls = new_urls
            mails.extend(new_mail)
        self.mails = list(set(mails))
        logging.info("#Mails trouvés : ")
        #self.logTextBox.appendPlainText("#Mails trouvés : ")
        logging.info("\n".join(self.mails))
        #self.logTextBox.appendPlainText("\n".join(self.mails))
        self.label_3.setText("Nombre de mails trouvés : {}".format(len(self.mails)))

        
if __name__ == '__main__':

    app = QApplication(sys.argv)
    widget = Mainwidget()
    widget.show()
    sys.exit(app.exec_())
