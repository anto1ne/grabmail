# GrabMail  ![logo](edit-find.png)

Logiciel pour scraper des mails sur des pages web.


## Lancement du logiciel.

Pour lancer GrabMail, il faut tout d'abord le télécharger et décompresser l'archive.  
Le logiciel se lance à partir de la commande suivante.

`python3 GrabMail.py `