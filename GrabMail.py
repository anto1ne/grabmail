#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 25 21:38:53 2021

@author: antoine
"""

import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QTranslator, QLibraryInfo, QLocale

from controller.gui import Mainwidget


LOCALE = QLocale.system().name()

app = QApplication(sys.argv)
widget = Mainwidget()

translator_qt = QTranslator()
translator_qt.load(
    "qt_" + LOCALE,
    QLibraryInfo.location(
        QLibraryInfo.TranslationsPath))

translator_app = QTranslator()

app.installTranslator(translator_qt)
    

widget.show()
sys.exit(app.exec_())