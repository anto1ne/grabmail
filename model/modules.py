#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 25 17:42:43 2021

@author: antoine
"""



import re
import requests
import unicodedata

def save_list(filename, data):
    with open(filename, 'w',encoding='utf8') as export:
        for line in data:
            export.write('{}\n'.format(line))
                
def get_data(url):
    
    urls=[]
    mails=[]
    try :
        x=requests.get(url, headers={"content-type":"text"}, timeout=1)
        regex_url=r'href=[\'"]?([^\'" >]+)'
        text = unicodedata.normalize("NFKD", x.text)
        urls = re.findall(regex_url, text)
        #print("\n".join(urls))
        #regex_mails="""[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n\<]+"""
        regex_mails="[a-zA-Z-]+@[a-zA-Z-]+\.[a-zA-Z]{2,6}"
        text = unicodedata.normalize("NFKD", x.text)
        mails = re.findall(regex_mails, text)
        #print("\n".join(mails))
    except:
        pass
    return list(set(urls)), list(set(mails))

def fetch_data(url, n=0):
    urls=[url]
    mails=[]
    for n in range(0,n+1):
        print("#Niveau : ", n)
        new_mail=[]
        new_urls=[]
        for link in urls :
            print("Lien : ", link)
            urls_found, mails_found = get_data(link)
            new_mail.extend(mails_found)
            new_urls.extend(urls_found)
        print("Nouveaux liens : ", len(new_urls))
        print("Nouveaux mails : ", len(new_mail))
        print("")
        urls = new_urls
        mails.extend(new_mail)
    return list(set(mails))

if __name__ == '__main__':

    url = "https://www.laquadrature.net/nous/"
     
    mails = fetch_data(url, n=1)
